from django.shortcuts import render, redirect


def home(request, template='shotcollector/home.html'):
    context = {}
    return render(request, template, context)

def submissions(request):
    context = {}
    if request.method == 'POST':
        pass
        # return submissions_post(contest, context, request, template)
    else:
        return redirect(to=home)
