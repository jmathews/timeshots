from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'shotcollector.views.home', name='home'),
    url(r'submissions/$', 'shotcollector.views.submissions', name='submissions'),
)
